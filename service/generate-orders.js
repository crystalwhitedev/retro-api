import Constants from "../constants.js";
import dayjs from "dayjs";
import { nanoid } from "nanoid";

function getNextOrderDay(daysToAdd, startingDate = Constants.starting_date) {
  return dayjs(startingDate).add(daysToAdd, "day").format("YYYY-MM-DD");
}

function generateProductCode(companyName) {
  const companyAbbr = companyName
    .split(" ")
    .map((word) => word[0])
    .join("")
    .toUpperCase();
  const code = Math.floor(Math.random() * 10000);
  return companyAbbr + "-PROD-" + code.toString().padStart(5, "0");
}

function generateOrders(companies, lastOrderDates) {
  const orders = [];
  for (const company of companies) {
    const orderCount = Math.floor(Math.random() * 1000);

    for (let i = 0; i < orderCount; i++) {
      const costRange = Constants.companies[company.company_name].cost_range;
      const orderQty = Math.floor(Math.random() * 100);
      const unitCost =
        (
          Math.random() * (costRange[1] - costRange[0] + 1) +
          costRange[0]
        ).toFixed(2) * 1;

      // sample list [order_id, company_id, order_date, product_id, order_quantity, unit_cost, total_cost, received ]
      orders.push([
        nanoid(),
        company.company_id,
        getNextOrderDay(i + 1, lastOrderDates[company.company_id]),
        generateProductCode(company.company_name),
        orderQty,
        unitCost,
        (orderQty * unitCost).toFixed(2) * 1,
      ]);
    }
  }
  return orders;
}

export default generateOrders;
