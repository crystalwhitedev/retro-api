import Constants from "../constants.js";
import dayjs from "dayjs";
import { nanoid } from "nanoid";

function getReceivedDay(daysToAdd, orderedDate) {
  return dayjs(orderedDate).add(daysToAdd, "day").format("YYYY-MM-DD");
}

function generateReceiveOrders(unreceivedOrders) {
  const receive = [];
  for (const order of unreceivedOrders) {
    const lagDays = Math.floor(Math.random() * 10 + 1);
    const receiveQty = Math.floor(Math.random() * order["order_quantity"] + 1);
    const unitCost = order["unit_cost"] * 1;

    // sample list [order_receipt_id, order_id, receipt_date, receipt_quntity, receipt_amount, receipt_total  ]
    receive.push([
      nanoid(),
      order["order_id"],
      getReceivedDay(lagDays, order["order_date"]),
      receiveQty,
      unitCost,
      (receiveQty * unitCost).toFixed(2) * 1,
    ]);
  }
  return receive;
}

export default generateReceiveOrders;
