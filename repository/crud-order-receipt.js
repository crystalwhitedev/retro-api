import { StringBuilder } from "../util/string-util.js";

function CrudOrderReceipt(fastify) {
  this.create = function (orderReceipt) {
    // create order receipt
  };

  this.update = function (orderReceipt) {
    // update order receipt
  };

  this.delete = function (id) {
    // delete order receipt
  };

  this.getById = function (id) {
    // get order receipt by id
  };

  this.getAll = function () {
    // get all order receipts
  };

  this.createBulk = async function (orderReceipts) {
    const connection = await fastify.mysql.getConnection();
    const query = new StringBuilder();
    query.append(
      "INSERT INTO torder_receipt (order_receipt_id, order_id, receipt_date, receipt_quntity, receipt_amount, receipt_total) VALUES "
    );
    orderReceipts.forEach((order, index) => {
      query.append("(?, ?, ?, ?, ?, ?)");
      if (index < orderReceipts.length - 1) {
        query.append(",");
      }
    });

    await connection.query(
      query.toString(),
      orderReceipts.flat(),
      (err, results) => {
        if (err) {
          console.error(err);
          throw err;
        }
        return "Order receipt placed successfully";
      }
    );
    connection.release();
  };
}

export default CrudOrderReceipt;
