import { StringBuilder } from "../util/string-util.js";

function CrudOrder(fastify) {
  this.create = function (order) {
    console.log("Order created");
  };

  this.read = function (id) {
    console.log("Order read");
  };

  this.update = function (order) {
    console.log("Order updated");
  };

  this.delete = function (id) {
    console.log("Order deleted");
  };

  this.createOrders = async function (orders) {
    const connection = await fastify.mysql.getConnection();
    const query = new StringBuilder();
    query.append(
      "INSERT INTO torder (order_id, company_id, order_date, product_id, order_quantity, unit_cost, total_cost) VALUES "
    );
    orders.forEach((order, index) => {
      query.append("(?, ?, ?, ?, ?, ?, ?)");
      if (index < orders.length - 1) {
        query.append(",");
      }
    });

    await connection.query(query.toString(), orders.flat(), (err, results) => {
      if (err) {
        console.error(err);
        throw err;
      }
      return "Orders placed successfully";
    });
    connection.release();
  };

  this.unreceivedOrders = async function () {
    const connection = await fastify.mysql.getConnection();
    const query = new StringBuilder();
    query.append("SELECT                                ");
    query.append("  o.order_id,                         ");
    query.append("  o.company_id,                       ");
    query.append("  o.order_date,                       ");
    query.append("  o.order_quantity,                   ");
    query.append("  o.unit_cost                         ");
    query.append("FROM                                  ");
    query.append("  torder o                            ");
    query.append("LEFT JOIN torder_receipt r ON         ");
    query.append("  o.order_id = r.order_id             ");
    query.append("WHERE                                 ");
    query.append("  o.order_id IS NOT NULL              ");
    query.append("  AND r.order_receipt_id IS NULL      ");
    query.append("ORDER BY                              ");
    query.append("  o.order_date                        ");

    const [rows, fields] = await connection.query(
      query.toSql(),
      [],
      (err, results) => {
        if (err) {
          console.error(err);
          throw err;
        }
        return results;
      }
    );
    connection.release();
    return rows;
  };
}

export default CrudOrder;
