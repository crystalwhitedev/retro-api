import Constants from "../constants.js";
import { StringBuilder } from "../util/string-util.js";

function CurdCompany(fastify) {
  this.create = async function (company) {};

  this.createList = function (companies) {};

  this.read = async function (id) {
    // read company
  };

  this.update = function (company) {
    // update company
  };

  this.delete = function (id) {
    // delete company
  };

  this.seedCompanies = async function () {
    const connection = await fastify.mysql.getConnection();

    const companies = Object.keys(Constants.companies).map((company, index) => {
      return [String(index + 1).padStart(5, "0"), company];
    });
    const query = new StringBuilder();
    query.append("INSERT INTO tcompany (company_id, company_name) VALUES ");
    companies.forEach((company, index) => {
      query.append("(?, ?)");
      if (index < companies.length - 1) {
        query.append(",");
      }
    });

    await connection.query(
      query.toString(),
      companies.flat(),
      (err, results) => {
        if (err) {
          console.error(err);
          throw err;
        }
        return "Companies seeded";
      }
    );
    connection.release();
  };

  this.loadAll = async function () {
    const connection = await fastify.mysql.getConnection();
    const query = "SELECT company_id, company_name FROM tcompany";
    const [rows, fields] = await connection.query(query);
    connection.release();
    return rows;
  };

  this.readLastOrderDate = async function (id) {
    const connection = await fastify.mysql.getConnection();
    const params = [];

    const query = new StringBuilder();
    query.append("SELECT                                    ");
    query.append("  o.company_id,                           ");
    query.append("  MAX(o.order_date) AS last_order_date    ");
    query.append("FROM                                      ");
    query.append("  torder o                                ");
    query.append("WHERE                                     ");
    query.append("  o.order_id IS NOT NULL                  ");
    if (id) {
      query.append("and company_id=?                        ");
      params.push(id);
    }
    query.append("GROUP BY                                  ");
    query.append("  o.company_id                            ");

    const companyLastOrderDates = {};
    const [rows, fields] = await connection.query(query.toSql(), params);
    connection.release();
    rows.forEach((row) => {
      companyLastOrderDates[row.company_id] = row.last_order_date;
    });
    return companyLastOrderDates;
  };
}

export default CurdCompany;
