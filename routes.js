import generateOrders from "./service/generate-orders.js";
import CrudCompany from "./repository/crud-company.js";
import CrudOrder from "./repository/crud-order.js";
import generateReceiveOrders from "./service/generate-receive-orders.js";
import CrudOrderReceipt from "./repository/crud-order-receipt.js";

async function routes(fastify, options) {
  fastify.get("/", async (request, reply) => {
    return { hello: "world" };
  });
  fastify.get("/place-orders", async (request, reply) => {
    const crudCompany = new CrudCompany(fastify);
    const lastOrderDays = await crudCompany.readLastOrderDate(null);
    const companies = await crudCompany.loadAll();
    const orders = generateOrders(companies, lastOrderDays);
    const crudOrder = new CrudOrder(fastify);
    await crudOrder.createOrders(orders);
    return { message: "Order placed successfully" };
  });

  fastify.get("/receive-orders", async (request, reply) => {
    const crudOrder = new CrudOrder(fastify);
    const unreceivedOrders = await crudOrder.unreceivedOrders();
    const receivedOrders = generateReceiveOrders(unreceivedOrders);
    const crudOrderReceipt = new CrudOrderReceipt(fastify);
    await crudOrderReceipt.createBulk(receivedOrders);
    return { message: "Order received successfully" };
  });

  fastify.get("/seed-companies", async (request, reply) => {
    const crudCompany = new CrudCompany(fastify);
    await crudCompany.seedCompanies();
    return { message: "Companies seeded" };
  });
}

export default routes;
