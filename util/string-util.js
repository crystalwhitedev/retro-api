export const StringAppend = (str1, str2) => {
  return str1 + str2;
};

export function StringBuilder(value) {
  this.strings = new Array();
  this.append(value);
}

StringBuilder.prototype.append = function (value) {
  if (value) {
    this.strings.push(value.trim());
  }
};

StringBuilder.prototype.clear = function () {
  this.strings.length = 0;
};

StringBuilder.prototype.toString = function () {
  return this.strings.join("");
};

StringBuilder.prototype.toSql = function () {
  return this.strings.join("\n");
};
