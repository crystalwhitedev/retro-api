create table tcompany (
	company_id varchar(50) PRIMARY KEY,
    company_name varchar(200) UNIQUE
);

create table torder (
    order_id varchar(50) PRIMARY KEY,
    company_id varchar(50),
    order_date date,
    product_id varchar(100),
    order_quantity int,
    unit_cost decimal(10,2),
    total_cost decimal(10,2),
    FOREIGN KEY (company_id) REFERENCES tcompany(company_id)
);

create table torder_receipt (
    order_receipt_id varchar(50) PRIMARY KEY,
    order_id varchar(50),
    receipt_date date,
    receipt_quntity int,
    receipt_amount decimal(10,2),
    receipt_total decimal(10,2),
    FOREIGN KEY (order_id) REFERENCES torder(order_id)
);