# RETRO-API
This is develped to load and fetch data for data engineering

## Author
[Pasan Jayawickrama](https://gitlab.com/bhadrajeepjaya)

## How to setup

```
yarn install

yarn run
```

## APIs

```
curl http://localhost:3000/seed-companies
curl http://localhost:3000/place-orders
curl http://localhost:3000/receive-orders
```