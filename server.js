import Fastify from "fastify";
const fastify = Fastify({ logger: true });

// Register DB connector
fastify.register(import("./db-connector.js"));
// Declare a route
fastify.register(import("./routes.js"));

try {
  await fastify.listen({ port: 3000 });
} catch (err) {
  fastify.log.error(err);
  process.exit(1);
}
