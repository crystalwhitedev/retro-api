const Constants = {
  companies: {
    "Aethon Robotics": { product: "Robot", cost_range: [5000, 50000] },
    "Lumina Labs": { product: "Lab Equipment", cost_range: [10000, 100000] },
    "Skybridge Solutions": { product: "Bridge Kit", cost_range: [5000, 20000] },
    "Solstice Energy": { product: "Solar Panel", cost_range: [500, 5000] },
    "Nebula Innovations": { product: "Space Suit", cost_range: [10000, 50000] },
    "Terraform Tech": {
      product: "Planetary Terraforming Kit",
      cost_range: [100000, 500000],
    },
    "Aqua Dynamics": {
      product: "Water Purification System",
      cost_range: [5000, 50000],
    },
    "Horizon Robotics": { product: "AI Chip", cost_range: [1000, 10000] },
    "Stellar Solutions": { product: "Telescope", cost_range: [5000, 50000] },
    "EcoTech Innovations": {
      product: "Eco-Friendly Home Kit",
      cost_range: [1000, 10000],
    },
    "Orbit Systems": { product: "Satellite", cost_range: [100000, 500000] },
    "Lunar Labs": { product: "Moon Rover", cost_range: [5000, 50000] },
    "Solaris Energy": {
      product: "Solar Power Generator",
      cost_range: [1000, 10000],
    },
    "AeroTech Solutions": { product: "Drone", cost_range: [500, 5000] },
    TerraLabs: { product: "Biodegradable Packaging", cost_range: [500, 5000] },
    "Hydro Dynamics": {
      product: "Hydroponic System",
      cost_range: [1000, 10000],
    },
    "Nebula Robotics": { product: "Space Rover", cost_range: [5000, 50000] },
    "EcoGen Innovations": {
      product: "Eco-Friendly Car",
      cost_range: [10000, 50000],
    },
    "AquaTech Solutions": {
      product: "Water Filtration System",
      cost_range: [5000, 50000],
    },
    "Horizon Innovations": {
      product: "AI Software",
      cost_range: [1000, 10000],
    },
    "Stellar Labs": { product: "Space Telescope", cost_range: [5000, 50000] },
    EcoSolutions: {
      product: "Eco-Friendly Home Kit",
      cost_range: [1000, 10000],
    },
    "Orbit Innovations": { product: "Satellite", cost_range: [100000, 500000] },
    "Lunar Robotics": { product: "Moon Rover", cost_range: [5000, 50000] },
    "Solar Energy Solutions": {
      product: "Solar Power Generator",
      cost_range: [1000, 10000],
    },
    AeroDynamics: { product: "Drone", cost_range: [500, 5000] },
    "TerraTech Labs": {
      product: "Biodegradable Packaging",
      cost_range: [500, 5000],
    },
    "HydroTech Solutions": {
      product: "Hydroponic System",
      cost_range: [1000, 10000],
    },
    "Nebula Robotics": { product: "Space Rover", cost_range: [5000, 50000] },
    "Verdant Haven": { product: "Eco-Friendly Product", cost_range: [5, 50] },
    "AquaTech Innovations": {
      product: "Water Filtration System",
      cost_range: [5000, 50000],
    },
  },
  starting_date: "2020-01-01",
};

export default Constants;
