import fp from "fastify-plugin";
import mysql from "@fastify/mysql";

async function mysqlConnector(fastify, options) {
  fastify.register(mysql, {
    promise: true,
    connectionString: "mysql://root:root@localhost:3306/retro_apis",
  });
}

export default fp(mysqlConnector);
